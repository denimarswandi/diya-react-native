import { useNavigation } from "@react-navigation/native";
import React, { useEffect, useLayoutEffect, useState } from "react";
import { SafeAreaView, View, Text, TouchableOpacity,Image, ScrollView, ActivityIndicator } from "react-native";
import { Attractions, Avatar, Hotels, Restaurants } from "../assets";
// import Snackbar from "react-native-snackbar";
import { Snackbar } from "react-native-paper";
import { GooglePlacesAutocomplete } from "react-native-google-places-autocomplete";
import MenuContainer from "../components/MenuContainer";
import { FontAwesome } from "@expo/vector-icons";
import ItemCarDontainer from "../components/ItemCarDontainer";
import { NotFound } from "../assets";
import { getPlaceData } from "../api";

function Discover() {
  const navigation = useNavigation();

  const [type, setType] = useState('restaurants')
  const [isLoading, setIsLoading] = useState(false)
  const [mainData, setmainData] = useState([])
  const [bl_lat, setBl_lat] = useState(null)
  const [bl_lng, setBl_lng] = useState(null)
  const [tr_lat, setTr_lat] = useState(null)
  const [tr_lng, setTr_lng] = useState(null)


  

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown:false
    });
  });

  useEffect(()=>{
    setIsLoading(true)
    getPlaceData(bl_lat, bl_lng, tr_lat, tr_lng).then(data=>{
      setmainData(data.data)
      setInterval(()=>{
        setIsLoading(false)
      },2000)
    })
  },[getPlaceData, bl_lat, bl_lng, tr_lat, tr_lng])

  

  return (
    <SafeAreaView className='flex-1 flex bg-white relative'>
      <View className='flex-row items-center justify-between mt-6 px-8'>
        <View>
          <Text className='text-[40px] text-[#0B646B] font-bold'>Discover</Text>
          <Text className='text-[#527283] text-[36px]'>the beauty today</Text>

        </View>
        <View className='w-12 h-12 bg-gray-400 rounded-md items-center justify-center shadow-lg'>
          <Image source={Avatar} className='w-full h-full rounded-md object-cover'></Image>
        </View>
      </View>
      <View className='flex-row items-center bg-white mx-4 py-1 px-4 shadow-lg mt-4'>
        <GooglePlacesAutocomplete placeholder="Search" GooglePlacesDetailsQuery={{fields:"geometry"}} fetchDetails={true} onPress={(data, details=null)=>{
          console.log(details?.geometry?.viewport)
          setBl_lat(details?.geometry?.viewport?.southwest?.lat)
          setBl_lng(details?.geometry?.viewport?.southwest?.lng)
          setTr_lat(details?.geometry?.viewport?.northeast?.lat)
          setBl_lng(details?.geometry?.viewport?.northeast?.lng)
        }} query={{
          key:'AIzaSyDYkfqQ4DH7asjdJZXVOMw4C7x7xxjQC0I',
          language:'en'
        }}/>
      </View>
      {isLoading ? <View className='flex-1 items-center justify-center'>
        <ActivityIndicator size="large" color="#0B646B"/>
      </View>:<ScrollView showsVerticalScrollIndicator={false}>
        <View className='flex-row items-center justify-between px-8 mt-8'>
          <MenuContainer key={"hotel"} title={"Hotels"} imageSrc={Hotels} type={type} setType={setType}/>
          <MenuContainer key={"attractions"} title={"Attractions"} imageSrc={Attractions} type={type} setType={setType}/>
          <MenuContainer key={"restaurants"} title={"Restaurants"} imageSrc={Restaurants} type={type} setType={setType}/>
          
        </View>
        <View className='flex-row items-center flex-wrap justify-between px-4 mt-8'>
         <Text className='text-[#2C7379] text-[28px] font-bold'>Top Tips</Text>
         <TouchableOpacity className='flex-row items-center justify-center space-x-2'>
          <Text className='text-[#A0C4C7] text-[20px] font-bold'>Explore</Text>
          <FontAwesome name="long-arrow-right" size={24} color="#A0C4C7"></FontAwesome>
         </TouchableOpacity>
        </View>
        <View className='px-4 mt-8 flex-row items-center justify-evenly flex-wrap'>
         {mainData.length > 0 ? 
         <>
         {mainData?.map((data, i)=>(
          <ItemCarDontainer key={i} imageSrc={data?.photo?.images?.medium?.url} title={data.name} location={data?.location_string} data={data}/>
         ))}
         </>:
         <>
         <View className='w-full h-[300px] items-center space-y-8 justify-center'>
          <Image source={NotFound} className='w-32 h-32 object-cover'></Image>
          <Text className='text-2xl text-[#428288] font-semibold'>Ooops No Data Found</Text>
         </View>
         </>}
        </View>
      </ScrollView>}
      
    </SafeAreaView>
  );
}

export default Discover;
