import { useNavigation } from "@react-navigation/native";
import React, { useLayoutEffect, useState } from "react";
import {
  View,
  Text,
  SafeAreaView,
  Image,
  TouchableOpacity
} from "react-native";
import { HeroImage } from "../assets";
import * as Animatable from 'react-native-animatable'


function HomeScreen() {
  const navigation = useNavigation();

  useState(()=>{
    console.log('ok')
  },[])
  


  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
  }, []);

  const press = () => {
    // navigation.navigate('Discover')
    navigation.replace('Discover')
  };



  return (
    <SafeAreaView className="bg-white flex-1 relative">
      <View className="mt-10 px-6 flex-row items-center space-x-2">
        <View className="w-16 h-16 bg-black rounded-full items-center justify-center">
          <Text className="text-[#00BCC9] text-3xl font-semibold">Go</Text>
        </View>
        <Text className="text-[#2A2B4B] text-3xl font-semibold">Travel</Text>
      </View>
      <View className="px-6 mt-8 space-y-3">
        <Text className="text-[#3C6072] text-[42px]">Enjoy the trip with</Text>
        <Text className="text-[#00BCC9] text-[38px] font-bold">
          Good Moments
        </Text>
        <Text className="text-[#3C6072] text-base">
          Lorem arstnar sniot arsitn eaniros arstneia rstionar tarsiotna
        </Text>
      </View>
      <View className="w-[400px] h-[400px] bg-[#00BCC9] rounded-full absolute bottom-36 -right-36"></View>
      <View className="w-[400px] h-[400px] bg-[#E99265] rounded-full absolute -bottom-28 -left-36"></View>
      <View className="flex-1 relative items-center justify-center">
        <Animatable.Image
          source={HeroImage}
          animation='fadeIn'
          easing='ease-in-out'

          className="w-full h-full mt-20 object-cover"
        />
        <View className="absolute bottom-20 w-24 h-24 border-l-2 border-r-2 border-t-4 border-[#00BCC9] rounded-full items-center justify-center">
        <TouchableOpacity onPress={press}>
          <Animatable.View animation='pulse' easing='ease-in-out' iterationCount='infinite' className="w-20 h-20 items-center justify-center rounded-full bg-[#00BCC6]">
            <Text className="text-gray-50 text-[36px] font-semibold">Go</Text>
          </Animatable.View>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
}

export default HomeScreen;
