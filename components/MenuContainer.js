import React from 'react'
import {View, Text, TouchableOpacity, Image} from 'react-native'

function MenuContainer({title, imageSrc, type, setType}) {
  const handlePress = ()=>{
    setType(title.toLowerCase())
  }
  return (
    <TouchableOpacity className='items-center justify-center space-y-2' onPress={handlePress}>
        <View className={`w-24 h-24 p-2 shadow-sm rounded-full items-center justify-center ${type===title.toLowerCase() ? "bg-gray-200":""}`}>
            <Image source={imageSrc} className='w-full h-full object-cover'/>
        </View>
        <Text className='text-[#00BCC6] text-xl'>{title}</Text>
    </TouchableOpacity>
  )
}

export default MenuContainer