import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { TailwindProvider } from 'tailwindcss-react-native';
import { View, Text, SafeAreaView } from 'react-native';
import HomeScreen from './screens/HomeScreen';
import * as SplashScreen from 'expo-splash-screen'
import React,{useEffect, useCallback, useState} from 'react';
import Discover from './screens/Discover';
import ItemScreen from './screens/ItemScreen';





const Stack = createNativeStackNavigator()

export default function App() {

  const [appIsReady, setAppIsReady] = useState(false)
  useEffect(()=>{
    async function prepare(){
       try{
        await new Promise(res=>setTimeout(res, 2000))
       }catch(e){
        console.warn(e)
       }finally{
        setAppIsReady(true)  
       }
    }
    prepare()
  },[])

  const onLayoutRootView = useCallback(async()=>{
    console.log(appIsReady)
    if(appIsReady){
        await SplashScreen.hideAsync();
    }
  }, [appIsReady])

  if(!appIsReady){
    return null
  }
  return (
   <TailwindProvider>
    <NavigationContainer >
      <Stack.Navigator >
        <Stack.Screen name='Home' component={HomeScreen} onLayout={onLayoutRootView}/>
        <Stack.Screen name="Discover" component={Discover}/>
        <Stack.Screen name="ItemScreen" component={ItemScreen}/>
      </Stack.Navigator >
    </NavigationContainer>
    
   </TailwindProvider>
  );
}

