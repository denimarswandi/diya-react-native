import axios from 'axios';

export const getPlaceData = async (bl_lat, bl_lng, tr_lat, tr_lng)=>{
    try{
        let params = {
            bl_latitude: bl_lat ? bl_lat:'11.847676',
            tr_latitude: tr_lat? tr_lat:'12.838442',
            bl_longitude: bl_lng? bl_lng:'109.095887',
            tr_longitude: tr_lng? tr_lng:'109.149359',
            restaurant_tagcategory_standalone: '10591',
            restaurant_tagcategory: '10591',
            limit: '30',
            currency: 'USD',
            open_now: 'false',
            lunit: 'km',
            lang: 'en_US'
          }
          let headers = {
            'X-RapidAPI-Key': '2f476a5c4emsh6f0cd0b18764d5ap1886f2jsn69b469f54509',
            'X-RapidAPI-Host': 'travel-advisor.p.rapidapi.com'
          }
        const data = await axios.get('https://travel-advisor.p.rapidapi.com/restaurants/list-in-boundary',{params:params, headers:headers})
        return data.data
    }catch(e){
        return null
    }
}